import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator'
import { Container } from 'typedi'
import { UserRespository } from '../repositories/user.repo'
import { message } from '../common/message.constants'

@ValidatorConstraint({ name: 'customText', async: true })
export class IsUserNameExists implements ValidatorConstraintInterface {
    constructor(private _repository: UserRespository) {
        this._repository = Container.get(UserRespository)
    }
    async validate(userName: string) {
        const user = await this._repository.findByUserName(userName)
        return user ? false : true
    }
    defaultMessage() {
        return message.USERNAME_EXIST
    }
}
@ValidatorConstraint({ name: 'customText', async: true })
export class IsUserIdExists implements ValidatorConstraintInterface {
    constructor(private _repository: UserRespository) {
        this._repository = Container.get(UserRespository)
    }
    async validate(userId: string) {
        const user = await this._repository.findById(userId)
        return user ? true : false
    }
    defaultMessage() {
        return message.USERID_NOT_EXIST
    }
}

@ValidatorConstraint({ name: 'customText', async: false })
export class IsDate implements ValidatorConstraintInterface {
    validate(value: string) {
        const check = value.match(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/)
        return check ? true : false
    }
    defaultMessage() {
        return message.BIRTHDAY_VALID
    }
}
