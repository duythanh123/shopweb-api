import { BaseModel } from './base'
import { ObjectID } from 'mongodb'
import { Product } from './category'

export interface Table extends BaseModel {
    name?: string
    area?: AreaField
    code?: string
    status?: boolean
    carts?: CartCopy[]
}

export interface Area extends BaseModel {
    name?: string
    status?: string
}

export interface AreaField {
    id?: string
    name?: string
    status?: string
}

export interface CartCopy {
    id?: ObjectID
    product?: Product
    amount?: number
    note?: string
}
