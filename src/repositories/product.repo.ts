import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Product } from '../model/category'
/**
 * ProductRespository
 * @author thanh
 */
@Service()
export class ProductRespository extends BaseRepository {
    constructor() {
        super('product')
    }

    public async search(page: number, limit: number) {
        let options = {}
        const query = {}

        options = {
            projection: {
                id: 1,
                name: 1,
                price: 1,
                description: 1,
                status: 1,
                categoryId: 1,
                picture: 1
            },
            sort: { name: 1 },
            skip: page * limit,
            limit: limit
        }
        const result = await this.find(query, options)
        const count = await this.countAll(query)
        return { result: result, count: count }
    }

    public async findByName(name: string, page: number, limit: number) {
        let options = {}
        const query = { name: { $regex: name, $options: 'i' } }
        options = {
            projection: {
                id: 1,
                name: 1,
                price: 1,
                description: 1,
                status: 1,
                categoryId: 1,
                picture: 1
            },
            sort: { name: 1 },
            skip: page * limit,
            limit: limit
        }
        const result = await this.find(query, options)
        const count = await this.countAll(query)
        return { result: result, count: count }
    }

    public async findByCategoryId(categoryId: string, page: number, limit: number) {
        const query = { categoryId: categoryId }
        const options = {
            projection: {
                id: 1,
                name: 1,
                price: 1,
                description: 1,
                status: 1,
                categoryId: 1,
                picture: 1
            },
            sort: { name: 1 },
            skip: page * limit,
            limit: limit
        }
        const result = await this.find(query, options)
        const count = await this.countAll(query)
        return { result: result, count: count }
    }

    public async findById(id: string) {
        const options = {
            projection: {
                id: 1,
                name: 1,
                price: 1,
                description: 1,
                status: 1,
                categoryId: 1,
                picture: 1
            }
        }

        return await this.findOne(this.buildFilterById(id), options)
    }

    public async createProduct(input: Product) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async updateProduct(id: string, input: Product) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteProduct(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }

    public async deleteManyProduct(categoryId: string) {
        const query = {
            categoryId: categoryId
        }
        let result
        try {
            result = await this.deleteLogicMany(query)
        } catch {
            return null
        }
        return result?.modifiedCount !== 0 ? true : false
    }
}
