import {
    Get,
    JsonController,
    Res,
    Post,
    Body,
    Param,
    Delete,
    Patch,
    CurrentUser,
    QueryParam
} from 'routing-controllers'
import { Service } from 'typedi'
import { IsNotEmpty, IsOptional, IsString, MaxLength, Validator, IsIn, IsNumber } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole, level } from '../common/constants'
import { ItemsRespository } from '../repositories/items.repo'
import { Warehouses } from '../model/items'
import { WarehousesRespository } from '../repositories/warehouses.repo'
/**
 * Auth Controller
 * @author Thanh
 */
export class WarehousesPostRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(50)
    supplier: string
    @IsOptional()
    @MaxLength(10)
    code: string
    @IsOptional()
    description: string
    @IsNumber()
    @IsOptional()
    price: number
    @IsIn([1, 2])
    type: number
    @IsNotEmpty()
    items: ItemRequest[]
}

interface ItemRequest {
    id?: string
    name?: string
    code?: number
    description?: string
    price?: number
    amount?: number
}
export class WarehousesPatchRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(50)
    supplier: string
    @IsOptional()
    @MaxLength(10)
    code: string
    @IsOptional()
    description: string
    @IsNumber()
    @IsOptional()
    amount: number
    @IsNumber()
    @IsOptional()
    price: number
    @IsIn([1, 2])
    type: number
    @IsNotEmpty()
    items: string
}

const validator = new Validator()

@Service()
@JsonController('/warehouses')
export class WarehousesController extends BaseController {
    constructor(
        private _userRepo: UserRespository,
        private _itemsRepo: ItemsRespository,
        private _warehousesRepo: WarehousesRespository
    ) {
        super()
    }

    @Get('/')
    async getAll(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!page) {
            page = 0
        }
        if (!limit) {
            limit = 6
        }
        return this.successResponse(await this._warehousesRepo.search(page, limit), response)
    }

    @Get('/:param')
    async getDetail(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @QueryParam('item') item: string,
        @Param('param') param: string,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level3.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (item === 'date') {
            if (!page) {
                page = 0
            }
            if (!limit) {
                limit = 5
            }
            const dateNumber: string[] = param.split('-')
            const result = await this._warehousesRepo.searchByDate(dateNumber, page, limit)
            return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
        }
        const resutl = await this._warehousesRepo.findById(param)
        return resutl
            ? this.successResponse(resutl, response)
            : this.failureResponse(message.PRODUCT_NOT_FOUND, response)
    }

    @Post('/')
    async create(
        @CurrentUser({ required: true }) user: User,
        @Body() body: WarehousesPostRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level3.includes(userLogin.role)) return this.forbiddenResponse(response)
        const input: Warehouses = {
            type: body.type,
            code: body.code,
            description: body.description,
            supplier: body.supplier,
            price: body.price,
            items: body.items
        }
        const result = await this._warehousesRepo.createWarehouses(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: WarehousesPatchRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level3.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Warehouses = {}
        if (body.type) input.type = body.type
        if (body.supplier) input.supplier = body.supplier
        if (body.code) input.code = body.code
        if (body.description) input.description = body.description

        const result = await this._warehousesRepo.updateWarehouses(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level3.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._warehousesRepo.deleteWarehouses(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }
}
