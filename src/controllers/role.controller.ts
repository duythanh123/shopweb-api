import { Get, JsonController, Res, Post, Body, Param, Patch, CurrentUser, Delete } from 'routing-controllers'
import { Service } from 'typedi'
import { MaxLength, Validator, IsNotEmpty, IsNumber } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User, Role } from '../model/user'
import { userRole } from '../common/constants'
import { RoleRespository } from '../repositories/role.repo'
/**
 * Auth Controller
 * @author Thanh
 */
export class RoleRequest {
    @IsNotEmpty()
    @MaxLength(30)
    roleName?: string
    @IsNumber()
    level?: number
}

const validator = new Validator()

@Service()
@JsonController('/role')
export class RoleController extends BaseController {
    constructor(private _userRepo: UserRespository, private _roleRepo: RoleRespository) {
        super()
    }
    @Get('/')
    async getAll(@CurrentUser({ required: true }) user: User, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin && userLogin.role === 1) return this.unauthorizedResponse(response)

        return this.successResponse(await this._roleRepo.findAllRole(), response)
    }

    @Get('/:id')
    async getOneProduct(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @Param('id') id: string
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin) return this.forbiddenResponse(response)

        const resutl = await this._roleRepo.findById(id)
        return resutl
            ? this.successResponse(resutl, response)
            : this.failureResponse(message.PRODUCT_NOT_FOUND, response)
    }

    @Post('/')
    async create(@CurrentUser({ required: true }) user: User, @Body() body: RoleRequest, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin) return this.forbiddenResponse(response)

        const input: Role = {
            roleName: body?.roleName,
            level: body?.level
        }
        const result = await this._roleRepo.createRole(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: RoleRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin && id !== user.id) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Role = {}
        if (body?.roleName) input.roleName = body.roleName
        if (body?.level) input.level = body.level

        const result = await this._roleRepo.updateRole(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._roleRepo.deleteRole(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }
}
