const path = require('path')

module.exports = {
    mode: 'production',
    entry: {
        main: './src/index.ts',
        vendor: './src/vendor.ts'
    },
    target: 'node',
    // devtool: 'inline-source-map',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.js'] //resolve all the modules other than index.ts
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        [
                            '@babel/preset-env',
                            {
                                targets: {
                                    node: '8.10'
                                }
                            }
                        ]
                    ]
                }
            },
            {
                test: /\.ts?$/,
                use: 'ts-loader'
            }
        ]
    },
    resolveLoader: {
        modules: [__dirname + '/node_modules']
    }
}
