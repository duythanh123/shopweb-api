import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Category } from '../model/category'
/**
 * UserRespository
 * @author Thanh
 */
@Service()
export class CategoryRespository extends BaseRepository {
    constructor() {
        super('category')
    }

    public async findById(id: string) {
        const options = {
            projection: { password: false }
        }
        return await this.findOne(this.buildFilterById(id), options)
    }

    public async createCategory(input: Category) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async getAllCategory() {
        return await this.getAll()
    }

    public async updateCategory(id: string, input: Category) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteCategory(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }
}
export default CategoryRespository
