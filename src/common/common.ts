// export const UploadDir = 'home/uploads/work-api/upload/media/'
export const MediaPath = '/upload/media/'

const UploadDir = './upload/media/'

export function getUploadDir() {
    //  const os = require('os')
    return UploadDir
}

/**
 * Return true if object is not empty
 * @param object
 */
export function isObjectNotEmpty(object: object): boolean {
    return !(object === undefined || object === null || Object.keys(object).length === 0)
}

/**
 * Return true if array is not empty
 * @param array
 */
export function isArrayNotEmpty(array: Array<object> | string[]): boolean {
    return !(array === undefined || array === null || array.length === 0)
}

/**
 * Return true if array is not empty
 * @param value
 */
export const isStringNotEmpty = (value: string) => {
    return !(value === undefined || value === null || value.length === 0 || value.trim().length === 0)
}

/**
 * Return true if object is not empty
 * @param object
 */
export function convertIdField(object: object) {
    if (object.hasOwnProperty('_id')) {
        object['id'] = object['_id'].toHexString()
        delete object['_id']
    }
    return object
}
