import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Table, CartCopy } from '../model/area'
import * as mongo from 'mongodb'
/**
 * TableRespository
 * @author thanh
 */
@Service()
export class TableRespository extends BaseRepository {
    constructor() {
        super('table')
    }

    public async findById(id: string) {
        const options = {
            projection: {
                id: 1,
                name: 1,
                area: 1,
                code: 1,
                status: 1,
                carts: 1
            }
        }
        return await this.findOne(this.buildFilterById(id), options)
    }

    public async findByAreaId(areaId: string) {
        const query = { 'area.id': areaId }

        const options = {
            projection: {
                id: 1,
                name: 1,
                area: 1,
                code: 1,
                status: 1,
                carts: 1
            }
        }
        return await this.find(query, options)
    }

    public async createTable(input: Table) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async attachCart(input: CartCopy[], id: string) {
        const updateQuery = {
            $set: { updatedAt: new Date().getTime() },
            $push: { carts: { $each: input } }
        }
        const result = await this.getCollection().updateOne(this.buildFilterById(id), updateQuery)
        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteOneCart(cartId: string, id: string) {
        const updateQuery = {
            $set: { updatedAt: new Date().getTime() },
            $pull: { carts: { id: new mongo.ObjectID(cartId) } }
        }
        const result = await this.getCollection().updateOne(this.buildFilterById(id), updateQuery)
        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteCart(id: string) {
        const updateQuery = {
            $set: { updatedAt: new Date().getTime(), carts: [] }
        }
        const result = await this.getCollection().updateOne(this.buildFilterById(id), updateQuery)
        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async updateCart(input: CartCopy, cartId: string, id: string) {
        const updateQuery = {
            $set: { 'carts.$.amount': input.amount }
        }
        const query = {
            _id: new mongo.ObjectID(id),
            'carts.id': new mongo.ObjectID(cartId)
        }
        const result = await this.getCollection().updateOne(query, updateQuery)
        if (result?.modifiedCount === 0) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async updateTable(id: string, input: Table) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteTable(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }

    public async deleteManyTable(areaId: string) {
        const query = {
            'area.id': areaId
        }
        let result
        try {
            result = await this.deleteLogicMany(query)
        } catch {
            return null
        }
        return result?.modifiedCount !== 0 ? true : false
    }
}
