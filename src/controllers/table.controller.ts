import { Get, JsonController, Res, Post, Body, Param, Patch, CurrentUser, Delete } from 'routing-controllers'
import { Service } from 'typedi'
import { IsOptional, MaxLength, Validator, IsBoolean, IsNotEmpty } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole, level } from '../common/constants'
import { AreaRespository } from '../repositories/area.repo'
import { TableRespository } from '../repositories/table.repo'
import { Table, CartCopy } from '../model/area'
import { ObjectID } from 'mongodb'
import { ProductRespository } from '../repositories/product.repo'
/**
 * Auth Controller
 * @author Thanh
 */
export class TableRequest {
    @IsOptional()
    @MaxLength(30)
    name?: string
    @IsOptional()
    @IsBoolean()
    status?: boolean
    @IsOptional()
    code?: string
    @IsOptional()
    areaId?: string
}

export class CartRequest {
    @IsNotEmpty()
    carts?: Cart[]
}

interface Cart {
    productId?: string
    amount?: number
    note?: string
}
const validator = new Validator()

@Service()
@JsonController('/table')
export class TableController extends BaseController {
    constructor(
        private _userRepo: UserRespository,
        private _areaRepo: AreaRespository,
        private _tableRepo: TableRespository,
        private _productRepo: ProductRespository
    ) {
        super()
    }
    @Get('/')
    async getAll(@CurrentUser({ required: true }) user: User, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        const tableList: Table[] = await this._tableRepo.getAll()
        for (let i = 0; i < tableList.length; i++) {
            tableList[i].area = await this._areaRepo.findById(tableList[i].area?.id)
        }
        return tableList
            ? this.successResponse(tableList, response)
            : this.failureResponse(message.PRODUCT_NOT_FOUND, response)
    }

    @Get('/:id')
    async getOneTable(@CurrentUser({ required: true }) user: User, @Res() response: Response, @Param('id') id: string) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        const result: Table = await this._tableRepo.findById(id)
        result ? (result.area = await this._areaRepo.findById(result.area?.id)) : null
        return result
            ? this.successResponse(result, response)
            : this.failureResponse(message.PRODUCT_NOT_FOUND, response)
    }

    @Get('/:areaId/area')
    async getTable(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @Param('areaId') areaId: string
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        const tableList: Table[] = await this._tableRepo.findByAreaId(areaId)
        for (let i = 0; i < tableList.length; i++) {
            tableList[i].area = await this._areaRepo.findById(tableList[i].area?.id)
        }
        return tableList
            ? this.successResponse(tableList, response)
            : this.failureResponse(message.PRODUCT_NOT_FOUND, response)
    }

    @Post('/')
    async create(@CurrentUser({ required: true }) user: User, @Body() body: TableRequest, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)
        if (!(await this._areaRepo.findById(body.areaId))) return this.failureResponse(message.AREA_NOT_EXSIT, response)
        const input: Table = {
            name: body.name,
            status: body.status,
            code: body.code,
            area: { id: body.areaId },
            carts: []
        }
        const result: Table = await this._tableRepo.createTable(input)
        result ? (result.area = await this._areaRepo.findById(result.area?.id)) : null
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Post('/:id/cart')
    async addCartToTable(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body() body: CartRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        const inputCart: Table = {
            carts: []
        }
        const tableRespo = await this._tableRepo.updateTable(id, inputCart)
        if (tableRespo) {
            const input: CartCopy[] = []
            for (let i = 0; i < body?.carts.length; i++) {
                const product = await this._productRepo.findById(body?.carts[i].productId)
                if (!product) return this.failureResponse(message.PRODUCT_NOT_EXSIT, response)
                const cart: CartCopy = {
                    id: new ObjectID(),
                    product: product,
                    amount: body?.carts[i].amount || 1
                }
                input.push(cart)
            }
            const result: Table = await this._tableRepo.attachCart(input, id)
            return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
        } else {
            return this.failureResponse(message.INSERT_FAIL, response)
        }
    }
    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: TableRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        if (!(await this._areaRepo.findById(body.areaId))) return this.failureResponse(message.AREA_NOT_EXSIT, response)
        const input: Table = {}
        if (body.name) input.name = body.name
        if (body.status) input.status = body.status
        if (body.code) input.code = body.code
        if (body.areaId) input.area = { id: body.areaId }

        const result: Table = await this._tableRepo.updateTable(id, input)
        result ? (result.area = await this._areaRepo.findById(result.area?.id)) : null
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._tableRepo.deleteTable(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }

    @Delete('/:id/cart/:cartId')
    async deleteOneCart(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Param('cartId') cartId: string,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._tableRepo.deleteOneCart(cartId, id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }

    @Delete('/:id/cart')
    async deleteCart(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._tableRepo.deleteCart(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }
}
