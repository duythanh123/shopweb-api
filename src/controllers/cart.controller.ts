import { Get, JsonController, Res, Post, Body, Param, Delete, Patch, CurrentUser } from 'routing-controllers'
import { Service } from 'typedi'
import { IsNotEmpty, IsOptional, IsString, MaxLength, Validator } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole } from '../common/constants'
import { Cart } from '../model/cart'
import { CartRespository } from '../repositories/cart.repo'
import { Product } from '../model/category'
import { ProductRespository } from '../repositories/product.repo'
/**
 * Auth Controller
 * @author Thanh
 */
export class CartRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    productId: string
    @IsOptional()
    amount: number
    @IsOptional()
    note: string
}

const validator = new Validator()

@Service()
@JsonController('/cart')
export class CartController extends BaseController {
    constructor(
        private _userRepo: UserRespository,
        private _cartRepo: CartRespository,
        private _productRepo: ProductRespository
    ) {
        super()
    }

    @Get('/')
    async getAll(@CurrentUser({ required: true }) user: User, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)

        return this.successResponse(await this._cartRepo.getAll(), response)
    }

    @Get('/:id')
    async getDetail(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._cartRepo.findById(id)
        return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
    }

    @Post('/')
    async create(@CurrentUser({ required: true }) user: User, @Body() body: CartRequest, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        // if (userLogin.role !== userRole.Admin) return this.forbiddenResponse(response)
        const product: Product = await this._productRepo.findById(body?.productId)
        if (!product) return this.failureResponse(message.PRODUCT_NOT_FOUND, response)
        const input: Cart = {
            product: product,
            amount: body?.amount || 1,
            note: body.note
        }

        const result = await this._cartRepo.createCart(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: CartRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin && id !== user.id) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Cart = {}
        if (body.amount) input.amount = body.amount
        if (body.note) input.note = body.note

        const result = await this._cartRepo.updateCart(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._cartRepo.deleteCart(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }
}
