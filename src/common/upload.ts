import multer = require('multer')
import path from 'path'
import fs from 'fs'

const storageOptions: multer.DiskStorageOptions = {
    destination: function (req, file, callbackFn) {
        if (!fs.existsSync(path.resolve('./data'))) {
            fs.mkdirSync(path.resolve('./data'), { recursive: true })
        }
        callbackFn(null, path.resolve('./data'))
    },
    filename: function (req, file, callbackFn) {
        const extension = path.extname(file.originalname)
        const name = file.originalname.replace(extension, '').replace(/[^a-zA-Z0-9]/g, '-')
        callbackFn(null, new Date().getTime() + '_' + name + extension)
    }
}
export const fileImageOptions = {
    storage: multer.diskStorage(storageOptions),
    limits: {
        fieldNameSize: 255,
        fileSize: 1024 * 1024 * 5
    }
}

export const fileVideoOptions = {
    storage: multer.diskStorage(storageOptions),
    limits: {
        fieldNameSize: 255,
        fileSize: 1024 * 1024 * 20
    }
}