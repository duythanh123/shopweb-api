import {
    Get,
    JsonController,
    Res,
    Post,
    Body,
    Param,
    Patch,
    CurrentUser,
    Delete,
    QueryParam
} from 'routing-controllers'
import { Service } from 'typedi'
import { IsNotEmpty, IsOptional, IsString, MaxLength, Validator } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole, level } from '../common/constants'
import CategoryRespository from '../repositories/category.repo'
import { ProductRespository } from '../repositories/product.repo'
import { Product } from '../model/category'
/**
 * Auth Controller
 * @author Thanh
 */
export class ProductRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    name: string
    @IsOptional()
    price: number
    @IsOptional()
    @IsString()
    @MaxLength(200)
    categoryId: string
    @IsOptional()
    @IsString()
    description?: string
    @IsOptional()
    picture: string
}

const validator = new Validator()

@Service()
@JsonController('/product')
export class ProductController extends BaseController {
    constructor(
        private _categoryRepo: CategoryRespository,
        private _userRepo: UserRespository,
        private _productRepo: ProductRespository
    ) {
        super()
    }
    @Get('/')
    async getAll(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        if (!page) {
            page = 0
        }
        if (!limit) {
            limit = 12
        }
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level4.includes(userLogin.role)) return this.forbiddenResponse(response)

        return this.successResponse(await this._productRepo.search(page, limit), response)
    }

    @Get('/:param')
    async getOneProduct(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @QueryParam('item') item: string,
        @Param('param') param: string,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level4.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!page) {
            page = 0
        }
        if (!limit) {
            limit = 12
        }
        if (item === 'name') {
            const result = await this._productRepo.findByName(param, page, limit)
            return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
        }
        if (item === 'category') {
            const result = await this._productRepo.findByCategoryId(param, page, limit)
            return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
        }
        const resutl = await this._productRepo.findById(param)
        return resutl
            ? this.successResponse(resutl, response)
            : this.failureResponse(message.PRODUCT_NOT_FOUND, response)
    }

    @Post('/')
    async create(@CurrentUser({ required: true }) user: User, @Body() body: ProductRequest, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        const input: Product = {
            name: body.name,
            price: body.price,
            description: body.description,
            categoryId: body.categoryId,
            picture: body.picture
        }
        const result = await this._productRepo.createProduct(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: ProductRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Product = {}
        if (body.name) input.name = body.name
        if (body.price) input.price = body.price
        if (body.categoryId) input.categoryId = body.categoryId
        if (body.description) input.description = body.description
        if (body.picture) input.picture = body.picture

        const result = await this._productRepo.updateProduct(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._productRepo.deleteProduct(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }
}
