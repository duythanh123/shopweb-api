import {
    Get,
    JsonController,
    Res,
    Post,
    Body,
    Param,
    Delete,
    QueryParam,
    Patch,
    CurrentUser
} from 'routing-controllers'
import { Service } from 'typedi'
import {
    IsEmail,
    IsNotEmpty,
    Validate,
    IsOptional,
    IsString,
    MaxLength,
    Validator,
    IsAlphanumeric,
    IsNumberString,
    IsIn
} from 'class-validator'
import { hashSync, genSaltSync } from 'bcryptjs'
import { message } from '../common/message.constants'
import { IsUserNameExists } from '../validator/custom.validator'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole, level } from '../common/constants'
/**
 * Auth Controller
 * @author Thanh
 */
export class UserRequest {
    @IsNotEmpty()
    @IsEmail()
    @MaxLength(30)
    email: string
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    name: string
    @IsNotEmpty()
    @IsAlphanumeric()
    @MaxLength(16)
    @Validate(IsUserNameExists)
    userName: string
    @IsOptional()
    birthday: number
    @IsOptional()
    @IsNumberString()
    @MaxLength(15)
    tel: string
    @IsOptional()
    @IsString()
    @MaxLength(200)
    address: string
    @IsIn([1, 2, 3, 4])
    role: number
    @IsOptional()
    password: string
}

export class ConnectionRequest {
    @IsNotEmpty()
    @IsString()
    connectionId: string
}
const validator = new Validator()

@Service()
@JsonController('/users')
export class UserController extends BaseController {
    constructor(private _userRepo: UserRespository) {
        super()
    }

    @Get('/')
    async getAll(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        if (!page) {
            page = 0
        }
        if (!limit) {
            limit = 5
        }
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level3.includes(userLogin.role)) return this.forbiddenResponse(response)
        return this.successResponse(await this._userRepo.search(page, limit), response)
    }

    @Get('/:param')
    async getDetail(
        @CurrentUser({ required: true }) user: User,
        @Param('param') param: string,
        @Res() response: Response,
        @QueryParam('item') item: string,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)

        if (item === 'name') {
            if (!page) {
                page = 0
            }
            if (!limit) {
                limit = 5
            }
            const result = await this._userRepo.findByName(param, page, limit)
            return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
        }
        if (!validator.isMongoId(param)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._userRepo.findById(param)
        return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
    }

    @Post('/')
    async create(@Body() body: UserRequest, @Res() response: Response, @CurrentUser({ required: true }) user: User) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        const input: User = {
            name: body.name,
            userName: body.userName,
            email: body.email,
            tel: body.tel,
            address: body.address,
            birthday: body.birthday,
            password: hashSync(body.password, genSaltSync(10)),
            role: body.role
        }
        const result = await this._userRepo.createUser(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: UserRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: User = {}
        if (body.name) input.name = body.name
        if (body.email) input.email = body.email
        if (body.address) input.address = body.address
        if (body.hasOwnProperty('birthday')) input.birthday = body.birthday
        if (body.hasOwnProperty('tel')) input.tel = body.tel
        if (body.role) input.role = body.role

        const result = await this._userRepo.updateUser(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)
        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._userRepo.deleteUser(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }
}
