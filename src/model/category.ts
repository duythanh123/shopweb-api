import { BaseModel } from './base'

export interface Product extends BaseModel {
    name?: string
    price?: number
    description?: string
    status?: string
    categoryId?: string
    picture?: string
}

export interface Category extends BaseModel {
    picture?: string
    name?: string
    status?: string
}
