import * as mongo from 'mongodb'
import { MongoHelper } from '../configuration/mongo.helper'
import { BaseModel } from '../model/base'
import { isObjectNotEmpty } from '../common/common'

const equalNull = { $eq: null }

export class BaseRepository {
    private _collection: mongo.Collection<object>

    constructor(collectionName: string) {
        this._collection = MongoHelper.getCollection(collectionName)
    }

    /**
     * This function is used to get _collection from name parameter
     * @param collectionName
     */
    protected getCollectionByName(collectionName): mongo.Collection<object> {
        return MongoHelper.getCollection(collectionName)
    }

    /**
     * This function is used to create filter with ObjectID
     * @param id
     */
    protected buildFilterById(id: string) {
        return { _id: new mongo.ObjectID(id) }
    }

    /**
     * This function is used to call aggregate function.
     * @param pipeline
     */
    protected async aggregate(pipeline: object[]): Promise<mongo.AggregationCursor<object>> {
        return this._collection.aggregate(pipeline)
    }

    protected async count(query: mongo.FilterQuery<BaseModel>) {
        query.deleteDate = equalNull
        return await this._collection.count(query)
    }

    /**
     * This function is used to find multiple documents.
     * @param query
     * @param options
     */
    protected async find(query: mongo.FilterQuery<BaseModel>, options?: mongo.FindOneOptions): Promise<BaseModel[]> {
        query.deleteDate = equalNull
        return (await this._collection.find<BaseModel>(query, options).toArray()).map(x => {
            x.id = x._id.toHexString()
            delete x._id
            return x
        })
    }

    /**
     * This function is used to find multiple documents.
     * @param query
     */
    public async countAll(query: mongo.FilterQuery<BaseModel>) {
        query.deleteDate = equalNull
        return await this._collection.find<BaseModel>(query).count()
    }

    /**
     * This function is used to find multiple documents.
     * @param query
     * @param options
     */
    public async getAll() {
        const query: mongo.FilterQuery<BaseModel> = {
            deleteDate: equalNull
        }
        return await this.find(query)
    }

    /**
     * This function is used to find multiple documents with collection name
     * @param collectionName
     * @param query
     * @param options
     */
    protected async findWithCollectionName(
        collectionName: string,
        query: mongo.FilterQuery<BaseModel>,
        options?: mongo.FindOneOptions
    ): Promise<BaseModel[]> {
        query.deleteDate = equalNull
        return (
            await this.getCollectionByName(collectionName)
                .find<BaseModel>(query, options)
                .toArray()
        ).map(x => {
            x.id = x._id.toHexString()
            delete x._id
            return x
        })
    }

    /**
     * This function is used to find single document.
     * @param query
     * @param options
     */
    protected async findOne(query: mongo.FilterQuery<BaseModel>, options?: mongo.FindOneOptions): Promise<BaseModel> {
        query.deleteDate = equalNull
        const result = await this._collection.findOne<BaseModel>(query, options)
        if (isObjectNotEmpty(result)) {
            result.id = result._id.toHexString()
            delete result._id
            return result
        }
        return null
    }

    /**
     * This function is used to insert one document.
     * @param input
     * @param options
     */
    protected async create(input: BaseModel, options?: mongo.CollectionInsertOneOptions) {
        const dateTime = new Date().getTime()
        input.createDate = dateTime
        input.updateDate = dateTime
        return this._collection.insertOne(input, options)
    }

    /**
     * This function is used to insert one document with collection name.
     * @param collectionName
     * @param input
     * @param options
     */
    protected async createWithCollectionName(
        collectionName: string,
        input: BaseModel,
        options?: mongo.CollectionInsertOneOptions
    ) {
        const dateTime = new Date().getTime()
        input.createDate = dateTime
        input.updateDate = dateTime
        return this.getCollectionByName(collectionName).insertOne(input, options)
    }

    /**
     * This function is used to update one document.
     * @param filter
     * @param update
     */
    protected async update(filter: mongo.FilterQuery<BaseModel>, update: BaseModel) {
        update.updateDate = new Date().getTime()
        const updateQuery = { $set: update }
        return this._collection.updateOne(filter, updateQuery)
    }

    /**
     * This function is used to update one document with param collection name
     * @param collectionName
     * @param filter
     * @param update
     */
    protected async updateWithCollectionName(
        collectionName: string,
        filter: mongo.FilterQuery<BaseModel>,
        update: BaseModel,
        multiUpdate?: boolean
    ) {
        update.updateDate = new Date().getTime()
        const updateQuery = { $set: update }
        const options = { multi: multiUpdate === true ? true : false }
        return this.getCollectionByName(collectionName).update(filter, updateQuery, options)
    }

    /**
     * This function is used to update one document.
     * @param filter
     * @param update
     */
    protected async addElementsToArray(filter: mongo.FilterQuery<BaseModel>, nestedArray: object) {
        const update: BaseModel = {
            updateDate: new Date().getTime()
        }
        const updateQuery = { $set: update, $push: nestedArray }
        return this._collection.updateOne(filter, updateQuery)
    }

    /**
     * This function is used to update one document.
     * @param filter
     * @param update
     */
    protected async removeElementFromArray(filter: mongo.FilterQuery<BaseModel>, nestedObject: object) {
        const update: BaseModel = {
            updateDate: new Date().getTime()
        }
        const updateQuery = { $set: update, $pull: nestedObject }
        return this._collection.updateOne(filter, updateQuery)
    }

    /**
     * This function is used to pull object from many elements
     * @param listId
     * @param nestedObject
     * @param collectionName
     */
    protected async bulkPullWithCollectionName(listId: string[], nestedObject: object, collectionName: string) {
        const bulk = this.getCollectionByName(collectionName).initializeUnorderedBulkOp()
        const updateQuery = { $pull: nestedObject }
        listId.forEach(id => {
            bulk.find({ _id: new mongo.ObjectID(id) }).update(updateQuery)
        })
        bulk.execute()
    }

    /**
     * This function is used to push an element to an array.
     * @param filter
     * @param array
     * @param element
     */
    protected async pushArray(filter: mongo.FilterQuery<BaseModel>, array: string, element: BaseModel) {
        const arrayQuery = {}
        arrayQuery[array] = element
        const updateQuery = { $push: arrayQuery }
        return this._collection.updateOne(filter, updateQuery)
    }

    /**
     * This function is used to push an element to an array.
     * @param filter
     * @param input
     */
    protected async pushReactionArray(filter: mongo.FilterQuery<BaseModel>, input: object) {
        const updateQuery = { $push: input }
        return this._collection.updateOne(filter, updateQuery)
    }

    /**
     * This function is used to delete logic.
     * @param filter
     */
    protected async deleteLogic(filter: mongo.FilterQuery<BaseModel>) {
        filter.deleteDate = equalNull
        const input: BaseModel = {
            deleteDate: new Date().getTime()
        }
        const updateQuery = { $set: input }
        return this._collection.updateOne(filter, updateQuery)
    }

    /**
     * This function is used to delete logic many item
     * @param filter
     */
    protected async deleteLogicMany(filter: mongo.FilterQuery<BaseModel>) {
        filter.deleteDate = equalNull
        const input: BaseModel = {
            deleteDate: new Date().getTime()
        }
        const updateQuery = { $set: input }
        return this._collection.updateMany(filter, updateQuery)
    }

    /**
     * This function is used to get _collection.
     */
    protected getCollection(): mongo.Collection<object> {
        return this._collection
    }
}
