import { Get, JsonController, Res, Post, Body, Param, Delete, Patch, CurrentUser } from 'routing-controllers'
import { Service } from 'typedi'
import { IsNotEmpty, IsOptional, IsString, MaxLength, Validator } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole, level } from '../common/constants'
import CategoryRespository from '../repositories/category.repo'
import { Category } from '../model/category'
import { ProductRespository } from '../repositories/product.repo'
/**
 * Auth Controller
 * @author Thanh
 */
export class CategoryRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    name: string
    @IsOptional()
    product: ProductRequest[]
    @IsOptional()
    status: string
}

export class ProductRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    name: string
    @IsOptional()
    price: number
    @IsOptional()
    status: string
}

const validator = new Validator()

@Service()
@JsonController('/category')
export class CategoryController extends BaseController {
    constructor(
        private _userRepo: UserRespository,
        private _categoryRepo: CategoryRespository,
        private _productRepo: ProductRespository
    ) {
        super()
    }

    @Get('/')
    async getAll(@CurrentUser({ required: true }) user: User, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level4.includes(userLogin.role)) return this.forbiddenResponse(response)
        return this.successResponse(await this._categoryRepo.getAll(), response)
    }

    @Get('/:id')
    async getDetail(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level4.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._categoryRepo.findById(id)
        return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
    }

    @Post('/')
    async create(
        @CurrentUser({ required: true }) user: User,
        @Body() body: CategoryRequest,
        @Res() response: Response
    ) {
        //@CurrentUser({ required: true }) user: User,
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        const input: Category = {
            name: body.name,
            status: body.status
        }

        const result = await this._categoryRepo.createCategory(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: CategoryRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Category = {}
        if (body.name) input.name = body.name
        if (body.status) input.status = body.status

        const result = await this._categoryRepo.updateCategory(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._categoryRepo.deleteCategory(id)
        if (result) {
            const responseProduct = await this._productRepo.deleteManyProduct(id)
            return responseProduct
                ? this.successResponse(null, response)
                : this.failureResponse(message.DELETE_FAIL, response)
        } else {
            this.failureResponse(message.DELETE_FAIL, response)
        }
    }
}
