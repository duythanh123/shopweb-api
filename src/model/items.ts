import { BaseModel } from './base'

export interface Items extends BaseModel {
    name?: string
    code?: string
    description?: string
    price?: number
}

export interface Warehouses extends BaseModel {
    type?: number
    supplier?: string
    code?: string
    items?: ItemsObject[]
    description?: string
    price?: number
}

export interface ItemsObject {
    id?: string
    name?: string
    code?: number
    description?: string
    price?: number
    amount?: number
}
