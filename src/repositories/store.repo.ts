import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Store } from '../model/store'
/**
 * UserRespository
 * @author thanh
 */
@Service()
export class StoreRespository extends BaseRepository {
    constructor() {
        super('store')
    }

    public async findById(id: string) {
        return await this.findOne(this.buildFilterById(id))
    }

    public async createStore(input: Store) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async updateStore(id: string, input: Store) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        const user = await this.findOne(this.buildFilterById(id))

        if (!input.name) return user

        let filter = {}
        filter['member.id'] = id
        let update = {}
        if (input.name) update['member.$.name'] = input.name
        await this.updateWithCollectionName('group', filter, update, true)

        update = {}
        if (input.name) update['member.$.name'] = input.name
        await this.updateWithCollectionName('project', filter, update, true)

        filter = {}
        filter['author.id'] = id
        update = {}
        if (input.name) update['author.name'] = input.name
        await this.updateWithCollectionName('group', filter, update, true)

        update = {}
        if (input.name) {
            update['author.name'] = input.name
            await this.updateWithCollectionName('project', filter, update, true)
        }

        return user
    }

    public async deleteUser(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }
}
