import { BaseModel } from './base'

export interface User extends BaseModel {
    name?: string
    userName?: string
    password?: string
    role?: number
    tel?: string
    email?: string
    birthday?: number
    address?: string
}

export interface Role extends BaseModel {
    roleName?: string
    level?: number
}
