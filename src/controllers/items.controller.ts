import {
    Get,
    JsonController,
    Res,
    Post,
    Body,
    Param,
    Delete,
    Patch,
    CurrentUser,
    QueryParam
} from 'routing-controllers'
import { Service } from 'typedi'
import { IsNotEmpty, IsOptional, IsString, MaxLength, Validator, IsIn } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole } from '../common/constants'
import { ItemsRespository } from '../repositories/items.repo'
import { Items } from '../model/items'
/**
 * Auth Controller
 * @author Thanh
 */
export class ItemsRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(50)
    name: string
    @IsOptional()
    @MaxLength(10)
    code: string
    @IsOptional()
    description: string
    @IsOptional()
    price: number
}

const validator = new Validator()

@Service()
@JsonController('/items')
export class ItemsController extends BaseController {
    constructor(private _userRepo: UserRespository, private _itemsRepo: ItemsRespository) {
        super()
    }

    @Get('/')
    async getAll(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!page) {
            page = 0
        }
        if (!limit) {
            limit = 5
        }
        return this.successResponse(await this._itemsRepo.search(page, limit), response)
    }

    @Get('/:param')
    async getDetail(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @QueryParam('item') item: string,
        @Param('param') param: string,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)

        if (item === 'name') {
            if (!page) {
                page = 0
            }
            if (!limit) {
                limit = 5
            }
            const result = await this._itemsRepo.findByName(param, page, limit)
            return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
        }
        const resutl = await this._itemsRepo.findById(param)
        return resutl
            ? this.successResponse(resutl, response)
            : this.failureResponse(message.PRODUCT_NOT_FOUND, response)
    }

    @Post('/')
    async create(@CurrentUser({ required: true }) user: User, @Body() body: ItemsRequest, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        // if (userLogin.role !== userRole.Admin) return this.forbiddenResponse(response)
        const input: Items = {
            name: body.name,
            code: body.code,
            description: body.description,
            price: body.price
        }

        const result = await this._itemsRepo.createItems(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: ItemsRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin && id !== user.id) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Items = {}
        if (body.name) input.name = body.name
        if (body.code) input.code = body.code
        if (body.description) input.description = body.description
        if (body.price) input.price = body.price

        const result = await this._itemsRepo.updateItems(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._itemsRepo.deleteItems(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }
}
