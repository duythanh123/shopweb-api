import 'reflect-metadata'
import express, { Request, Response } from 'express'
import fs from 'fs'
import path from 'path'
import http from 'http'
import { useContainer, Action, useExpressServer } from 'routing-controllers'
import { VerifyToken } from './security/jwt'
import { isObjectNotEmpty, MediaPath, getUploadDir } from './common/common'
import { Container } from 'typedi'
import { MongoHelper } from './configuration/mongo.helper'
import { ExceptionMiddleware } from './middleware/exception.middleware'
import { RequestMiddleware } from './middleware/request.middleware'
import { UserController } from './controllers/user.controller'
import { AuthController } from './controllers/auth.controller'
import { UploadController } from './controllers/upload.controller'
import { StoreController } from './controllers/store.controller'
import { CategoryController } from './controllers/category.controller'
import { ProductController } from './controllers/product.controller'
import { TableController } from './controllers/table.controller'
import { AreaController } from './controllers/area.controller'
import { RoleController } from './controllers/role.controller'
import { CartController } from './controllers/cart.controller'
import { BillController } from './controllers/bill. controller'
import { ItemsController } from './controllers/items.controller'
import { WarehousesController } from './controllers/warehouses.controller'

/**
 * Setup routing-controllers to use typedi container.
 */
useContainer(Container)

const expressApp = express()

const responseImage = (imagePath: string, res: Response) => {
    try {
        const bytes = fs.readFileSync(path.resolve(imagePath))
        res.contentType('png')
        res.send(bytes)
    } catch (error) {
        res.status(404).json('File not found')
    }
}

const responseVideo = (videoPath: string, req: Request, res: Response) => {
    try {
        const pathMedia = path.resolve(videoPath)
        const stat = fs.statSync(pathMedia)
        const fileSize = stat.size
        const range = req.headers.range
        if (range) {
            const parts = range.replace(/bytes=/, '').split('-')
            const start = parseInt(parts[0], 10)
            const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1
            const chunksize = end - start + 1
            const file = fs.createReadStream(pathMedia, { start, end })
            const head = {
                'Content-Range': `bytes ${start}-${end}/${fileSize}`,
                'Accept-Ranges': 'bytes',
                'Content-Length': chunksize,
                'Content-Type': 'video/mp4'
            }
            res.writeHead(206, head)
            file.pipe(res)
        } else {
            const head = {
                'Content-Length': fileSize,
                'Content-Type': 'video/mp4'
            }
            res.writeHead(200, head)
            fs.createReadStream(pathMedia).pipe(res)
        }
    } catch (error) {
        res.status(404).json('File not found')
    }
}

expressApp.use(`${MediaPath}:id`, (req: Request, res: Response) => {
    if (req.originalUrl.includes('video')) {
        responseVideo(getUploadDir() + req.path, req, res)
    } else {
        if (req.path === '/') {
            responseImage(getUploadDir() + req.params.id, res)
        } else {
            responseImage(getUploadDir() + req.params.id + req.path, res)
        }
    }
})

const app = useExpressServer(expressApp, {
    cors: true,
    defaultErrorHandler: false,
    routePrefix: '/shopping',
    authorizationChecker: async (action: Action, roles: string[]) => {
        const token = action.request.headers['authorization']?.split(' ')[1]

        const user = VerifyToken(token) as object
        return isObjectNotEmpty(user)
    },
    currentUserChecker: async (action: Action) => {
        const token = action.request.headers['authorization']?.split(' ')[1]
        return VerifyToken(token)
    },
    controllers: [
        AuthController,
        UserController,
        UploadController,
        StoreController,
        CategoryController,
        ProductController,
        TableController,
        AreaController,
        RoleController,
        CartController,
        BillController,
        ItemsController,
        WarehousesController
    ],
    middlewares: [RequestMiddleware, ExceptionMiddleware]
})

const server = http.createServer(app)
const PORT = process.env.PORT || 30000
// const mongoURI = `mongodb://chatwork-dev:chatwork-dev@54.169.187.159:27017/chatwork-dev`
const mongoURI = process.env.MONGODB_URI || `mongodb://localhost:27017/shopping`
MongoHelper.connect(mongoURI)
    .then(() => {
        console.info(`> Connected to Mongo!`)
        server.listen(PORT, () => {
            console.log('> Ready on http://localhost:' + PORT)
        })
    })
    .catch(error => {
        console.error('> Unable to connecto to MongoDB!', error)
    })
