import { Get, JsonController, Res, Post, Body, Param, Patch, CurrentUser } from 'routing-controllers'
import { Service } from 'typedi'
import { IsNotEmpty, IsOptional, IsString, MaxLength, Validator, IsNumberString } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole, level } from '../common/constants'
import { Store } from '../model/store'
import { StoreRespository } from '../repositories/store.repo'
/**
 * Auth Controller
 * @author Thanh
 */
export class StoreRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    name: string
    @IsOptional()
    @IsNumberString()
    @MaxLength(15)
    tel: string
    @IsOptional()
    @IsString()
    @MaxLength(200)
    address: string
    @IsOptional()
    avatar: string
    @IsString()
    description?: string
    @IsOptional()
    @IsNotEmpty()
    timeOpen: number
    @IsOptional()
    @IsNotEmpty()
    timeClose: number
}

const validator = new Validator()

@Service()
@JsonController('/store')
export class StoreController extends BaseController {
    constructor(private _storeRepo: StoreRespository, private _userRepo: UserRespository) {
        super()
    }

    @Get('/')
    async getAll(@CurrentUser({ required: true }) user: User, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        const resutl = await this._storeRepo.getAll()
        return resutl
            ? this.successResponse(resutl[0], response)
            : this.failureResponse(message.STORE_NOT_FOUND, response)
    }

    @Post('/')
    async create(@CurrentUser({ required: true }) user: User, @Body() body: StoreRequest, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin) return this.forbiddenResponse(response)

        const input: Store = {
            name: body.name,
            tel: body.tel,
            address: body.address,
            avatar: body.avatar,
            description: body.description,
            timeOpen: body.timeOpen,
            timeClose: body.timeClose
        }
        const result = await this._storeRepo.createStore(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: StoreRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (userLogin.role !== userRole.Admin && id !== user.id) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Store = {}
        if (body.name) input.name = body.name
        if (body.address) input.address = body.address
        if (body.avatar) input.avatar = body.avatar
        if (body.description) input.description = body.description
        if (body.hasOwnProperty('tel')) input.tel = body.tel
        if (body.timeClose) input.timeClose = body.timeClose
        if (body.timeOpen) input.timeOpen = body.timeOpen

        const result = await this._storeRepo.updateStore(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }
}
