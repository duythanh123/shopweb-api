import { Get, JsonController, Res, Post, Body, Param, Patch, CurrentUser, Delete } from 'routing-controllers'
import { Service } from 'typedi'
import { IsOptional, MaxLength, Validator } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { userRole, level } from '../common/constants'
import { AreaRespository } from '../repositories/area.repo'
import { Area } from '../model/area'
import { TableRespository } from '../repositories/table.repo'
/**
 * Auth Controller
 * @author Thanh
 */
export class AreaRequest {
    @IsOptional()
    @MaxLength(30)
    name?: string
    @IsOptional()
    status?: string
}

const validator = new Validator()

@Service()
@JsonController('/area')
export class AreaController extends BaseController {
    constructor(
        private _userRepo: UserRespository,
        private _areaRepo: AreaRespository,
        private _tableRepo: TableRespository
    ) {
        super()
    }
    @Get('/')
    async getAll(@CurrentUser({ required: true }) user: User, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        return this.successResponse(await this._areaRepo.getAll(), response)
    }

    @Get('/:id')
    async getOneProduct(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @Param('id') id: string
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        const resutl = await this._areaRepo.findById(id)
        return resutl
            ? this.successResponse(resutl, response)
            : this.failureResponse(message.PRODUCT_NOT_FOUND, response)
    }

    @Post('/')
    async create(@CurrentUser({ required: true }) user: User, @Body() body: AreaRequest, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        const input: Area = {
            name: body.name,
            status: body.status
        }
        const result = await this._areaRepo.createArea(input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.INSERT_FAIL, response)
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: AreaRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)
        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Area = {}
        if (body.name) input.name = body.name
        if (body.status) input.status = body.status

        const result = await this._areaRepo.updateArea(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level2.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._areaRepo.deleteArea(id)
        if (result) {
            const responseProduct = await this._tableRepo.deleteManyTable(id)
            return responseProduct
                ? this.successResponse(null, response)
                : this.failureResponse(message.DELETE_FAIL, response)
        } else {
            this.failureResponse(message.DELETE_FAIL, response)
        }
    }
}
