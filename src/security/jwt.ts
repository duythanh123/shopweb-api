import * as jwt from 'jsonwebtoken'
const secretKey = process.env.MYSECRETKEY || 'mySecretKey'

/**
 * To generate JWT from user credential
 * @param {*} user
 * @returns JWT string
 */
export function GenerateToken(user: { id: string; userName: string; password: string }) {
    const payload = {
        id: user.id,
        userName: user.userName,
        password: user.password
    }
    const options: jwt.SignOptions = {
        algorithm: 'HS384',
        expiresIn: 3600 * 24 // expires in 1 hours
    }
    const token: string = jwt.sign(payload, secretKey, options)
    return token
}
/**
 * To verify JWT string from requests
 * @param {*} token
 * @returns user credential
 */
// verify Token
export function VerifyToken(token: string): string | object {
    return jwt.verify(token, secretKey)
}
