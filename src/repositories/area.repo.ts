import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Area } from '../model/area'
/**
 * AreaRespository
 * @author thanh
 */
@Service()
export class AreaRespository extends BaseRepository {
    constructor() {
        super('area')
    }

    public async findById(id: string) {
        return await this.findOne(this.buildFilterById(id))
    }

    public async createArea(input: Area) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async updateArea(id: string, input: Area) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteArea(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }
}
