import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Role } from '../model/user'

/**
 * AreaRespository
 * @author thanh
 */
@Service()
export class RoleRespository extends BaseRepository {
    constructor() {
        super('role')
    }

    public async findById(id: string) {
        return await this.findOne(this.buildFilterById(id))
    }

    public async findAllRole() {
        const options = {
            projection: {
                id: 1,
                roleName: 1,
                level: 1
            },
            sort: { level: 1 }
        }
        return await this.find({}, options)
    }

    public async createRole(input: Role) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async updateRole(id: string, input: Role) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteRole(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }
}
