export const issueTracker = Object.freeze({
    1: 'Task',
    2: 'Bug'
})
export const issueStatus = Object.freeze({
    New: 1,
    Started: 2,
    Finished: 3,
    Closed: 4,
    Canceled: 5,
    Reopened: 6
})

export const userRole = Object.freeze({
    Admin: 1,
    Manager: 2,
    Cashier: 3,
    Waiter: 4
})

export const level = Object.freeze({
    level1: [1],
    level2: [1, 2],
    level3: [1, 2, 3],
    level4: [1, 2, 3, 4]
})

export const action = Object.freeze({
    get: 'Get',
    insert: 'Insert',
    update: 'Update',
    delete: 'Delete'
})
