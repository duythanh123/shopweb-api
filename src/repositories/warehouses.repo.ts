import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Warehouses } from '../model/items'
/**
 * WarehousesRespository
 * @author thanh
 */
@Service()
export class WarehousesRespository extends BaseRepository {
    constructor() {
        super('warehouses')
    }

    public async search(page: number, limit: number) {
        let options = {}
        const query = {}

        options = {
            projection: {
                id: 1,
                type: 1,
                items: 1,
                code: 1,
                supplier: 1,
                price: 1,
                description: 1,
                createDate: 1
            },
            skip: page * limit,
            limit: limit,
            sort: { createDate: -1 }
        }
        const result = await this.find(query, options)
        const count = await this.countAll(query)
        return { result: result, count: count }
    }

    public async searchByDate(dateNumber: string[], page: number, limit: number) {
        const query = { createDate: { $gte: parseInt(dateNumber[0]), $lte: parseInt(dateNumber[1]) + 100000000 } }
        const options = {
            projection: {
                id: 1,
                type: 1,
                items: 1,
                code: 1,
                price: 1,
                description: 1,
                supplier: 1,
                createDate: 1
            },
            skip: page * limit,
            limit: limit,
            sort: { createDate: -1 }
        }
        const result = await this.find(query, options)
        const count = await this.countAll(query)
        return { result: result, count: count }
    }

    public async findById(id: string) {
        const options = {
            projection: {
                id: 1,
                type: 1,
                items: 1,
                code: 1,
                price: 1,
                supplier: 1,
                description: 1,
                createDate: 1
            }
        }

        return await this.findOne(this.buildFilterById(id), options)
    }

    public async createWarehouses(input: Warehouses) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops) : null
    }

    public async updateWarehouses(id: string, input: Warehouses) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteWarehouses(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }
}
