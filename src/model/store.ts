import { BaseModel } from './base'

export interface Store extends BaseModel {
    name?: string
    tel?: string
    avatar?: string
    description?: string
    timeOpen?: number
    timeClose?: number
    address?: string
}
