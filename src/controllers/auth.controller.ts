import { Response } from 'express'
import { JsonController, Res, Post, Body, Get, Patch } from 'routing-controllers'
import { IsNotEmpty, IsString, MaxLength } from 'class-validator'
import bcrypt from 'bcryptjs'
import { Service } from 'typedi'
import { GenerateToken } from '../security/jwt'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { CurrentUser } from 'routing-controllers'
import { User } from '../model/user'
import { message } from '../common/message.constants'
/**
 * Auth Controller
 * @author Eli
 */

const salt = bcrypt.genSaltSync(10)

export class LoginRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(16)
    userName: string
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    password: string
}

export class ChangePasswordRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    oldPassword: string
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    newPassword: string
}

@Service()
@JsonController('/auth')
export class AuthController extends BaseController {
    constructor(private _repository: UserRespository) {
        super()
    }

    @Post('/login')
    async login(@Body() body: LoginRequest, @Res() response: Response) {
        const result = (await this._repository.findByUserName(body.userName)) as User
        if (result) {
            const compare = bcrypt.compareSync(body.password, result['password'])
            if (compare) {
                const user = {
                    id: result.id,
                    userName: result.userName,
                    password: result.password
                }
                const token = GenerateToken(user)
                result['token'] = token
                delete result['password']
                return this.successResponse(result, response)
            } else {
                return this.loginFailResponse(response)
            }
        } else {
            return this.loginFailResponse(response)
        }
    }

    @Patch('/changepassword')
    async changePassword(
        @CurrentUser({ required: true }) user: User,
        @Body() body: ChangePasswordRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._repository.findByUserName(user.userName)
        if (!userLogin) return this.unauthorizedResponse(response)

        const compare = bcrypt.compareSync(body.oldPassword, userLogin.password)
        if (!compare) return this.failureResponse(message.INCCORECT_PASSWORD, response)

        const input = { password: bcrypt.hashSync(body.newPassword, salt) }
        const result = await this._repository.updateUser(user.id, input)
        return this.successResponse(result, response)
    }

    @Get('/validate')
    async validateToken(@CurrentUser({ required: true }) user: User, @Res() response: Response) {
        return this.successResponse(user, response)
    }
}
