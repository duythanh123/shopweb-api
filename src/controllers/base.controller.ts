import { Response } from 'express'
import { Controller } from 'routing-controllers'

@Controller()
export class BaseController {
    protected successResponse(result: object | string, response: Response) {
        return response.status(200).json(result)
    }
    protected loginFailResponse(response: Response) {
        return response.status(401).json({ message: 'Invalid Username or Password' })
    }
    protected failureResponse(message: string, response: Response) {
        return response.status(202).json({ message: message })
    }
    protected forbiddenResponse(response: Response) {
        return response.status(403).json({ message: 'You have not permission to access.' })
    }
    protected unauthorizedResponse(response: Response) {
        return response.status(401).json({ message: 'Unauthorized' })
    }
    protected DataFailResponse(response: Response) {
        return response.status(500).json({ message: 'Data get fail' })
    }
}
