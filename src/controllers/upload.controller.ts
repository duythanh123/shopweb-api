/* eslint-disable @typescript-eslint/camelcase */
import { Response } from 'express'
import { JsonController, Res, Post, UploadedFile } from 'routing-controllers'
import { Service } from 'typedi'
import { BaseController } from './base.controller'
import { MediaPath, getUploadDir } from '../common/common'
import multer = require('multer')
import path from 'path'
import fs from 'fs'
import { v2 } from 'cloudinary'

/**
 * UploadController Controller
 * @author thanh
 */
const currentDate = Date.now()

v2.config({
    cloud_name: 'ds5zmvcyj',
    api_key: '518299291746627',
    api_secret: 'Fy_T4jN5XpWh61OQh455oOZSsFs'
})

const uploads = file => {
    return new Promise(resolve => {
        v2.uploader.upload(file, (error, result) => {
            resolve(result?.url)
        })
    })
}

const storageOptions: multer.DiskStorageOptions = {
    destination: function(req, _, callbackFn) {
        const dir = path.resolve(`${getUploadDir()}${currentDate}/`)
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true })
        }
        callbackFn(null, dir)
    },
    filename: function(req, file, callbackFn) {
        const extension = path.extname(file.originalname)
        const name = file.originalname.replace(extension, '').replace(/[^a-zA-Z0-9]/g, '-')
        callbackFn(null, `${new Date().getTime()}_` + name + extension)
    }
}

const fileOptions = {
    storage: multer.diskStorage(storageOptions),
    limits: {
        fieldNameSize: 255,
        fileSize: 1024 * 1024 * 25 // 5 MB
    }
}

@Service()
@JsonController('/upload')
export class UploadController extends BaseController {
    /**
     * Upload file
     * @param file: open from browser
     * @param response: file path after upload to server
     */
    @Post('/')
    async upload(@UploadedFile('file', { options: fileOptions }) file: any, @Res() response: Response) {
        if (file['mimetype'].includes('video')) {
            return this.successResponse(`${MediaPath}${currentDate}/${file['filename']}`, response)
        } else {
            const result = await uploads(file['path'])
            return this.successResponse({ url: result }, response)
        }
    }
}
