import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { User } from '../model/user'
/**
 * UserRespository
 * @author Thanh
 */
@Service()
export class UserRespository extends BaseRepository {
    constructor() {
        super('user')
    }

    public async findById(id: string) {
        const options = {
            projection: { password: false }
        }
        return await this.findOne(this.buildFilterById(id), options)
    }

    public async createUser(input: User) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async updateUser(id: string, input: User) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        const user = await this.findOne(this.buildFilterById(id))

        if (!input.name) return user

        let filter = {}
        filter['member.id'] = id
        let update = {}
        if (input.name) update['member.$.name'] = input.name
        await this.updateWithCollectionName('group', filter, update, true)

        update = {}
        if (input.name) update['member.$.name'] = input.name
        await this.updateWithCollectionName('project', filter, update, true)

        filter = {}
        filter['author.id'] = id
        update = {}
        if (input.name) update['author.name'] = input.name
        await this.updateWithCollectionName('group', filter, update, true)

        update = {}
        if (input.name) {
            update['author.name'] = input.name
            await this.updateWithCollectionName('project', filter, update, true)
        }

        return user
    }

    public async deleteUser(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }

    public async search(page: number, limit: number) {
        let options = {}
        const query = {}

        options = {
            projection: {
                id: 1,
                userName: 1,
                name: 1,
                email: 1,
                address: 1,
                birthday: 1,
                tel: 1,
                createDate: 1,
                role: 1
            },
            sort: { name: 1 },
            skip: page * limit,
            limit: limit
        }
        const result = await this.find(query, options)
        const count = await this.countAll(query)
        return { result: result, count: count }
    }

    public async findByName(name: string, page: number, limit: number) {
        let options = {}
        const query = { name: { $regex: name, $options: 'i' } }
        options = {
            projection: {
                id: 1,
                userName: 1,
                name: 1,
                email: 1,
                address: 1,
                birthday: 1,
                tel: 1,
                createDate: 1,
                role: 1
            },
            sort: { name: 1 },
            skip: page * limit,
            limit: limit
        }
        const result = await this.find(query, options)
        const count = await this.countAll(query)
        return { result: result, count: count }
    }

    public async findByUserName(userName: string) {
        const filter = {
            userName: userName
        }
        return await this.findOne(filter)
    }
}
