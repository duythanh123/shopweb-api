import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Bill } from '../model/bill'

/**
 * BillRespository
 * @author thanh
 */
@Service()
export class BillRespository extends BaseRepository {
    constructor() {
        super('bill')
    }

    public async search(page: number, limit: number) {
        const query = {}

        const options = {
            projection: {
                id: 1,
                totalPrice: 1,
                table: 1,
                createDate: 1
            },
            sort: { createDate: -1 },
            skip: page * limit,
            limit: limit
        }
        const result = await this.find(query, options)
        const count = await this.countAll(query)
        return { result: result, count: count }
    }

    public async searchByDate(dateNumber: string[], page: number, limit: number) {
        const query = { createDate: { $gte: parseInt(dateNumber[0]), $lte: parseInt(dateNumber[1]) + 100000000 } }

        const options = {
            projection: {
                id: 1,
                totalPrice: 1,
                table: 1,
                createDate: 1
            },
            sort: { createDate: -1 },
            skip: page * limit,
            limit: limit
        }
        let totalPrice = 0
        try {
            const result = await this.find(query, options)
            const billTotal: Bill[] = await this.find(query)
            billTotal.map(bill => {
                totalPrice += bill?.totalPrice
            })
            const count = await this.countAll(query)
            return { result: result, count: count, total: totalPrice }
        } catch (error) {
            return null
        }
    }

    public async searchAllByDate(dateNumber: string[]) {
        const query = { createDate: { $gte: parseInt(dateNumber[0]), $lte: parseInt(dateNumber[1]) + 100000000 } }

        const options = {
            projection: {
                id: 1,
                totalPrice: 1,
                table: 1,
                createDate: 1
            },
            sort: { createDate: -1 }
        }
        try {
            const result = await this.find(query, options)
            return { result: result }
        } catch (error) {
            return null
        }
    }

    public async findById(id: string) {
        return await this.findOne(this.buildFilterById(id))
    }

    public async createBill(input: Bill) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async updateBill(id: string, input: Bill) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteBill(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }
}
