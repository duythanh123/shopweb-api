import { BaseModel } from './base'
import { Table } from './area'

export interface Bill extends BaseModel {
    totalPrice?: number
    table?: Table
}
