import { BaseModel } from './base'
import { Product } from './category'

export interface Cart extends BaseModel {
    product?: Product
    amount?: number
    note?: string
}
