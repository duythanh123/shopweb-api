export const message = Object.freeze({
    // Response message
    NOT_FOUND: 'Not Found',
    STORE_NOT_FOUND: 'Store Not Found',
    USER_NOT_FOUND: 'User Not Found',
    PRODUCT_NOT_FOUND: 'Product Not Found',
    MESSAGE_NOT_FOUND: 'Messsage Not Found',
    ID_VALID: 'ID is invalid',
    INSERT_FAIL: 'Insert failed',
    UPDATE_FAIL: 'Update failed',
    DELETE_FAIL: 'Delete failed',
    SEQUENCE_FAIL: 'Create number failed',
    UPDATE_NOTHING: 'Nothing to update',
    DELETE_NOTHING: 'Nothing to delete',
    INCCORECT_PASSWORD: 'Old password is incorrect',

    // Validator message
    BIRTHDAY_VALID: 'birthday is invalid (must be DD/MM/YYYY)',
    USERNAME_EXIST: 'userName exists',
    USERID_NOT_EXIST: 'userId does not exists',
    PRIVATE_GROUP_MEMBER_VALID: 'Private chat cannot contain more than two members',
    PRIVATE_GROUP_REMOVE_MEMBER: 'Cannot remove member of a private group chat',
    CANNOT_UPDATE_LAST_MESSAGE_GROUP: 'Cannot update last message to group',
    MESSAGE_NOT_BELONG_TO_USER: 'This message does not belong to user',
    DUEDATE_VALID: 'dueDate must be greater than or equal startDate',
    MESSAGE_IS_EMPTY: 'content of message should not be empty',
    PERCENT_VALID: 'Status or PercentDone is invalid',
    AREA_NOT_EXSIT: 'Area not exsit',
    PRODUCT_NOT_EXSIT: 'Product not exsit'
})
