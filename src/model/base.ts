import { ObjectId } from 'mongodb'

export interface BaseModel {
    _id?: ObjectId
    id?: string
    createDate?: number
    updateDate?: number
    deleteDate?: number
}
