import { BaseRepository } from './base.repo'
import { Service } from 'typedi'
import { convertIdField } from '../common/common'
import { Cart } from '../model/cart'

/**
 * AreaRespository
 * @author thanh
 */
@Service()
export class CartRespository extends BaseRepository {
    constructor() {
        super('cart')
    }

    public async findById(id: string) {
        const options = {
            projection: {
                id: 1,
                product: 1,
                note: 1,
                amount: 1
            }
        }
        return await this.findOne(this.buildFilterById(id), options)
    }

    public async createCart(input: Cart) {
        let result
        try {
            result = await this.create(input)
        } catch {
            return null
        }
        return result?.insertedCount === 1 ? convertIdField(result?.ops[0]) : null
    }

    public async updateCart(id: string, input: Cart) {
        let result
        try {
            result = await this.update(this.buildFilterById(id), input)
        } catch {
            return null
        }

        if (result?.modifiedCount !== 1) return null
        return await this.findOne(this.buildFilterById(id))
    }

    public async deleteCart(id: string) {
        let result
        try {
            result = await this.deleteLogic(this.buildFilterById(id))
        } catch {
            return null
        }
        return result?.modifiedCount === 1 ? true : false
    }
}
