import {
    Get,
    JsonController,
    Res,
    Post,
    Body,
    Param,
    Delete,
    Patch,
    CurrentUser,
    QueryParam
} from 'routing-controllers'
import { Service } from 'typedi'
import { IsNotEmpty, IsOptional, IsString, MaxLength, Validator } from 'class-validator'
import { message } from '../common/message.constants'
import { Response } from 'express'
import { BaseController } from './base.controller'
import { UserRespository } from '../repositories/user.repo'
import { User } from '../model/user'
import { level } from '../common/constants'
import { BillRespository } from '../repositories/bill.repo'
import { CartRespository } from '../repositories/cart.repo'
import { Bill } from '../model/bill'
import { TableRespository } from '../repositories/table.repo'
import { Table } from '../model/area'
/**
 * Auth Controller
 * @author Thanh
 */
export class BillRequest {
    @IsOptional()
    totalPrice: number
    @IsNotEmpty()
    tableId: string
}

export class ProductRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(30)
    name: string
    @IsOptional()
    price: number
    @IsOptional()
    status: string
}

const validator = new Validator()

@Service()
@JsonController('/bill')
export class BillController extends BaseController {
    constructor(
        private _userRepo: UserRespository,
        private _billRepo: BillRespository,
        private _cartRepo: CartRespository,
        private _tableRepo: TableRespository
    ) {
        super()
    }

    @Get('/')
    async getAll(
        @CurrentUser({ required: true }) user: User,
        @Res() response: Response,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level3.includes(userLogin.role)) return this.forbiddenResponse(response)
        if (!page) {
            page = 0
        }
        if (!limit) {
            limit = 10
        }

        return this.successResponse(await this._billRepo.search(page, limit), response)
    }

    @Get('/:param')
    async getDetail(
        @CurrentUser({ required: true }) user: User,
        @Param('param') param: string,
        @QueryParam('item') item: string,
        @QueryParam('page') page: number,
        @QueryParam('limit') limit: number,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level3.includes(userLogin.role)) return this.forbiddenResponse(response)
        if (item === 'date') {
            if (!page && !limit) {
                const dateNumber: string[] = param.split('-')
                const result = await this._billRepo.searchAllByDate(dateNumber)
                return result
                    ? this.successResponse(result, response)
                    : this.failureResponse(message.NOT_FOUND, response)
            }
            const dateNumber: string[] = param.split('-')
            const result = await this._billRepo.searchByDate(dateNumber, page, limit)
            return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
        }

        const result = await this._billRepo.findById(param)
        return result ? this.successResponse(result, response) : this.failureResponse(message.NOT_FOUND, response)
    }

    @Post('/')
    async create(@CurrentUser({ required: true }) user: User, @Body() body: BillRequest, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level4.includes(userLogin.role)) return this.forbiddenResponse(response)

        const input: Bill = {
            table: null,
            totalPrice: body.totalPrice
        }

        const tableRes: Table = await this._tableRepo.findById(body.tableId)
        if (tableRes) input.table = tableRes

        const result = await this._billRepo.createBill(input)
        if (result) {
            const inputCart: Table = {
                carts: []
            }
            const tableRespo = await this._tableRepo.updateTable(body.tableId, inputCart)
            return tableRespo
                ? this.successResponse(tableRespo, response)
                : this.failureResponse(message.INSERT_FAIL, response)
        } else {
            this.failureResponse(message.INSERT_FAIL, response)
        }
    }

    @Patch('/:id')
    async update(
        @CurrentUser({ required: true }) user: User,
        @Param('id') id: string,
        @Body({ validate: { skipMissingProperties: true, forbidUnknownValues: true } }) body: BillRequest,
        @Res() response: Response
    ) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        //if (userLogin.role !== userRole.Admin && id !== user.id) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const input: Bill = {}
        if (body.totalPrice) input.totalPrice = body.totalPrice

        const result = await this._billRepo.updateBill(id, input)
        return result ? this.successResponse(result, response) : this.failureResponse(message.UPDATE_FAIL, response)
    }

    @Delete('/:id')
    async delete(@CurrentUser({ required: true }) user: User, @Param('id') id: string, @Res() response: Response) {
        const userLogin: User = await this._userRepo.findById(user.id)
        if (!userLogin) return this.unauthorizedResponse(response)
        if (!level.level3.includes(userLogin.role)) return this.forbiddenResponse(response)

        if (!validator.isMongoId(id)) {
            return this.failureResponse(message.ID_VALID, response)
        }
        const result = await this._billRepo.deleteBill(id)
        return result ? this.successResponse(null, response) : this.failureResponse(message.DELETE_FAIL, response)
    }
}
